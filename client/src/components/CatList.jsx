import React from "react";
import Cat from "./Cat";

const CatList = ({ cats, handleCatClick }) => {
  return (
    <>
      {cats.map((cat) => (
        <Cat cat={cat} handleCatClick={handleCatClick} />
      ))}
    </>
  );
};

export default CatList;
