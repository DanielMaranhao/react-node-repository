import React from "react";
import "./Cat.css";

const Cat = ({ cat, handleCatClick }) => {
  if (cat.isRevealed === false) {
    return null;
  } else {
    return (
      <div className="cat-container" onClick={() => handleCatClick(cat.id)}>
        <img
          className="cat-image"
          src={`https://cdn2.thecatapi.com/images/${cat.reference_image_id}.jpg`}
          alt={cat.name}
        />
        <div className="cat-text">
          <h2>{cat.name}</h2>
          <h4>Origin: {cat.origin}</h4>
          <p>Description: </p>
          <p>{cat.description}</p>
          {cat.expanded && (
            <>
              <p>
                <strong>Temperament: </strong>
                {cat.temperament}
              </p>
              <p>
                <strong>Adaptability: </strong>
                {cat.adaptability}
              </p>
              <p>
                <strong>Intelligence: </strong>
                {cat.intelligence}
              </p>
              <p>
                <strong>Wikipedia Link: </strong>
                <a
                  href={cat.wikipedia_url}
                  onClick={(e) => {
                    e.stopPropagation();
                  }}
                >
                  {cat.wikipedia_url}
                </a>
              </p>
            </>
          )}
        </div>
      </div>
    );
  }
};

export default Cat;
