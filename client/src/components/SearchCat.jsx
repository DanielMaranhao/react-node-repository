import React, { useState } from "react";

import "./SearchCat.css";

const SearchCat = ({ handleCatSearch }) => {
  const [inputData, setInputData] = useState("");

  const handleInputChange = (e) => {
    setInputData(e.target.value);
    handleCatSearch(e.target.value);
  };  

  return (
    <div className="cat-search-container">      
      <input
        onChange={handleInputChange}
        value={inputData}
        className="cat-search-input"
        type="text"
      />      
    </div>
  );
};

export default SearchCat;
