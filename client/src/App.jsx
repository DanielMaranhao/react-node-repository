import React, { useEffect, useState } from "react";
import axios from "axios";

import CatList from "./components/CatList";
import SearchCat from "./components/SearchCat";

import "./App.css";

const App = () => {
  const [cats, setCats] = useState([]);

  useEffect(() => {
    const fetchCats = async () => {
      const { data } = await axios.get("https://api.thecatapi.com/v1/breeds");

      setCats(data);
    };

    fetchCats();
  }, []);

  const handleCatClick = (catId) => {
    const updatedCatList = cats.map((cat) => {
      if (cat.id === catId) return { ...cat, expanded: !cat.expanded };

      return cat;
    });

    setCats(updatedCatList);
  };

  const handleCatSearch = (catName) => {
    const updatedCatList = cats.map((cat) => {
      if (cat.name.toUpperCase().includes(catName.toUpperCase())) {
        return { ...cat, isRevealed: true };
      } else {
        return { ...cat, isRevealed: false };
      }
    });

    setCats(updatedCatList);
  };

  // const [data, setData] = useState(null);

  // useEffect(() => {
  //   fetch("/api")
  //     .then((res) => res.json())
  //     .then((data) => setData(data.message));
  // }, []);

  return (
    <div className="container">
      <SearchCat handleCatSearch={handleCatSearch} />
      <CatList cats={cats} handleCatClick={handleCatClick} />
      {/* <p>{!data ? "Loading..." : data}</p> */}
    </div>
  );
};

export default App;
